package no.ntnu.henrbjoh.mappe3.patientRegister;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class PatientManagerTest {
    static PatientManager patientManager;

    /**
     * Method that creates a new fileHandler and therefore a file for testData in the resources-dictionary
     * It then adds two Patients to the file
     */
    @BeforeAll
    public static void initialize(){
        try {
            File file = new File("src/main/resources/no/ntnu/henrbjoh/mappe3/patientRegister/testFiles/patientManagerTest.csv");
            file.delete();
            patientManager = new PatientManager("src/main/resources/no/ntnu/henrbjoh/mappe3/patientRegister/testFiles/patientManagerTest.csv");
            patientManager.addNewPatient(new Patient("Henrik","Johnsrud","Kåre","10101022332"));
            patientManager.addNewPatient(new Patient("Nils","Martin","Thomas","11221122112"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks if all the data in the file created in initialize() is correct
     */
    @Test
    @Order(1)
    void getPatients() {
        ArrayList<Patient> patients = patientManager.getPatients();
        int successes = 0;

        if (patients.get(0).getFirstName().equals("Henrik")){successes++;}
        if (patients.get(0).getLastName().equals("Johnsrud")){successes++;}
        if (patients.get(0).getGeneralPartitioner().equals("Kåre")){successes++;}
        if (patients.get(0).getSocialSecurityNumber().equals("10101022332")){successes++;}

        if (patients.get(1).getFirstName().equals("Nils")){successes++;}
        if (patients.get(1).getLastName().equals("Martin")){successes++;}
        if (patients.get(1).getGeneralPartitioner().equals("Thomas")){successes++;}
        if (patients.get(1).getSocialSecurityNumber().equals("11221122112")){successes++;}
        assertEquals(8,successes);
    }

    /**
     * Adds a new patient to the Arraylist in the patientManager and checks if its length has been increased
     */
    @Test
    @Order(2)
    void addNewPatient() {
        patientManager.addNewPatient(new Patient("William", "Carlsen", "Robert","12211221122"));
        assertEquals(patientManager.getPatients().size(),3);
    }

    /**
     * Removes the Patient added in the last test (addNewPatient()) and checks if the list of patients in patientManager is now correct
     */
    @Test
    @Order(3)
    void removePatient() {
        patientManager.removePatient("12211221122");
        assertEquals(patientManager.getPatients().size(),2);
    }

    /**
     * Saves the patients in the patient-list in patientManager to file, and checks that the file is not blank
     * If it had been blank the savePatientsToFile would fail, because in initialize() the class only adds the
     * patients to the ArrayList in patientManager, not the file.
     */
    @Test
    @Order(4)
    void savePatientsToFile() {
        try {
            patientManager.savePatientsToFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        File saveFile = new File("src/main/resources/no/ntnu/henrbjoh/mappe3/patientRegister/testFiles/patientManagerTest.csv");
        assertTrue(saveFile.length()!=0);
    }

    /**
     * Created an exportFile in resources, if it exists it will delete the file, and export the data in patientManager to the file
     * then checks if the new file exists and is exported
     */
    @Test
    @Order(5)
    void exportFile() {
        File exportFile = new File("src/main/resources/no/ntnu/henrbjoh/mappe3/patientRegister/testFiles/patientManagerTestExport.csv");
        if (exportFile.exists()){
            exportFile.delete();
        }
        try {
            patientManager.exportFile(exportFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertTrue(exportFile.exists());
    }

    /**
     * Imports a the exportFile from exportFile(), (export will be ran before this test so the file will exist)
     * and checks if the data in the patientsList is updated to the new length
     */
    @Test
    @Order(6)
    void importFile() {
        File importFile = new File("src/main/resources/no/ntnu/henrbjoh/mappe3/patientRegister/testFiles/patientManagerTestExport.csv");
        try {
            patientManager.importFile(importFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(5, patientManager.getPatients().size());
    }
}