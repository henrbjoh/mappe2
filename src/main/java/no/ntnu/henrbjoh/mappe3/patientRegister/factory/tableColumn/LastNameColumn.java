package no.ntnu.henrbjoh.mappe3.patientRegister.factory.tableColumn;

public class LastNameColumn implements TableColumn{
    @Override
    public String getTableColumn() {
        return "lastNameColumn";
    }
}
