package no.ntnu.henrbjoh.mappe3.patientRegister;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main-class that will run the program
 */
public class Main extends Application {
    /**
     * Here the FXML-file is loaded and the program-window is shown
     * @param stage the main-stage which the program runs on
     * @throws Exception If the loading of the FXML fails. The user will get an error-message
     */
    @Override
    public void start(Stage stage) throws Exception {
        Parent main = FXMLLoader.load(getClass().getResource("scenes/main.fxml"));
        Scene scene = new Scene(main);
        stage.setScene(scene);
        stage.show();
    }
}
