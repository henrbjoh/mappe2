package no.ntnu.henrbjoh.mappe3.patientRegister.factory.tableView;

public class PatientTableView implements TableView{
    @Override
    public String getTableView(){
        return "patientTableView";
    }
}
