package no.ntnu.henrbjoh.mappe3.patientRegister;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * This class keeps track of all the patients in the register, and is an connection between the file and program
 */
public class PatientManager {
    private final Filehandler filehandler;
    private ArrayList<Patient> patients;

    /**
     * Constructor, connects the application up with the file that the data in application will be stored.
     * It is now, stored directly in the users, home-folder. As a hidden .csv-file
     * @param filePath path to where the data-file will be stored
     * @throws IOException if the File-handler is not able to connect up with the file
     */
    public PatientManager(String filePath) throws IOException {
        filehandler = new Filehandler(filePath);
        patients = filehandler.getDataAsPatientsFromAppData();
    }

    public ArrayList<Patient> getPatients() {
        return new ArrayList<>(patients);
    }

    /**
     * adds a new patient to the Arraylist with patients. Will not add if there are a patient with the same social security number already added
     * @param newPatient Patient object to be added
     * @return boolean, true if success, false if it failed
     */
    public boolean addNewPatient(Patient newPatient) {
        if (patients.size()==0){
            patients.add(newPatient);
            return true;
        }
        for (Patient p : patients) {
            if (!(newPatient.getSocialSecurityNumber() == p.getSocialSecurityNumber())) {
                patients.add(newPatient);
                return true;
            }
        }
        return false;
    }

    /**
     * Method for removing a patient
     *
     * @param SocialSecurityNumber, to the patient to be removed
     * @return true if the patient is removed, false if not
     */
    public boolean removePatient(String SocialSecurityNumber) {
        for (Patient p : patients) {
            if (p.getSocialSecurityNumber().equals(SocialSecurityNumber)) {
                patients.remove(p);
                return true;
            }
        }
        return false;
    }

    /**
     * Calls the file-handler to save all the patients in the programs list of patients to the data-file
     * @throws IOException if it can not save to file
     */
    public void savePatientsToFile() throws IOException {
        filehandler.savePatientsToFile(patients.toArray());
    }

    /**
     * Calls the file-handler to make an export of all the data in the program to a given .csv-file
     * @param exportFile The file which the data will be loaded to
     * @throws IOException if it can not export the file
     */
    public void exportFile(File exportFile) throws IOException {
        filehandler.exportToFile(patients.toArray(),exportFile);
    }

    /**
     * Gets a list of Patient-objects from file-handler, adds them to the patients list and saves it all to the data-file
     * @param importFile The file which the imported data is stored
     * @throws IOException if the import-file does not meet the characteristics of the program
     */
    public void importFile(File importFile) throws IOException{
        ArrayList<Patient> dataFromFile = filehandler.getDataAsPatientsFromImportFile(importFile);
        patients.addAll(dataFromFile);
        savePatientsToFile();
    }
}
