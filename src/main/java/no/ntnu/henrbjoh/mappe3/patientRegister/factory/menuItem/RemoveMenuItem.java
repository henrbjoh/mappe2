package no.ntnu.henrbjoh.mappe3.patientRegister.factory.menuItem;

public class RemoveMenuItem implements MenuItem{
    @Override
    public String getMenuItem() {
        return "remove";
    }
}
