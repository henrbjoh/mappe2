package no.ntnu.henrbjoh.mappe3.patientRegister.factory;

public interface AbstractFactory<T> {
    T create(String type);
}
