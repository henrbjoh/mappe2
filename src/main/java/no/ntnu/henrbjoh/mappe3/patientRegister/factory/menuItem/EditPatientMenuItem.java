package no.ntnu.henrbjoh.mappe3.patientRegister.factory.menuItem;

public class EditPatientMenuItem implements MenuItem{
    @Override
    public String getMenuItem() {
        return "editPatient";
    }
}
