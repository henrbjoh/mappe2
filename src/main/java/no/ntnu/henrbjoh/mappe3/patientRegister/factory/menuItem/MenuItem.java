package no.ntnu.henrbjoh.mappe3.patientRegister.factory.menuItem;

public interface MenuItem {
    String getMenuItem();
}
