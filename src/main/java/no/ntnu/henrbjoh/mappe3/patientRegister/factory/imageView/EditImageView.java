package no.ntnu.henrbjoh.mappe3.patientRegister.factory.imageView;

public class EditImageView implements ImageView{
    @Override
    public String getImageView(){
        return "editImageView";
    }
}
