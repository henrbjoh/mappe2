package no.ntnu.henrbjoh.mappe3.patientRegister.factory.imageView;

import no.ntnu.henrbjoh.mappe3.patientRegister.factory.AbstractFactory;
import no.ntnu.henrbjoh.mappe3.patientRegister.factory.menuItem.*;

public class ImageViewFactory implements AbstractFactory<ImageView> {
    @Override
    public ImageView create(String type) {
        if ("addImageView".equalsIgnoreCase(type)) {
            return new AddImageView();
        } else if ("editImageView".equalsIgnoreCase(type)) {
            return new EditImageView();
        } else if ("removeImageView".equalsIgnoreCase(type)) {
            return new RemoveImageView();
        }
        return null;
    }
}
