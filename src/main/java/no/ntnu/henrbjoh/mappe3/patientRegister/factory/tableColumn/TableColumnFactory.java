package no.ntnu.henrbjoh.mappe3.patientRegister.factory.tableColumn;

import no.ntnu.henrbjoh.mappe3.patientRegister.factory.AbstractFactory;

public class TableColumnFactory implements AbstractFactory<TableColumn> {
    @Override
    public TableColumn create(String type) {
        if ("firstNameColumn".equalsIgnoreCase(type)) {
            return new FirstNameColumn();
        } else if ("lastNameColumn".equalsIgnoreCase(type)) {
            return new LastNameColumn();
        } else if ("generalPractitionerColumn".equalsIgnoreCase(type)) {
            return new LastNameColumn();
        } else if ("SSNColumn".equalsIgnoreCase(type)) {
            return new LastNameColumn();
        }
        return null;
    }
}
