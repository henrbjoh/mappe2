package no.ntnu.henrbjoh.mappe3.patientRegister.factory.menuItem;

public class AboutMenuItem implements MenuItem{
    @Override
    public String getMenuItem() {
        return "about";
    }
}
