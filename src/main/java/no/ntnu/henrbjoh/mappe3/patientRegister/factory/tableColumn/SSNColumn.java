package no.ntnu.henrbjoh.mappe3.patientRegister.factory.tableColumn;

public class SSNColumn implements TableColumn{
    @Override
    public String getTableColumn() {
        return "SSNColumn";
    }
}
