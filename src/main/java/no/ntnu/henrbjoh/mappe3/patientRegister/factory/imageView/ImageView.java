package no.ntnu.henrbjoh.mappe3.patientRegister.factory.imageView;

public interface ImageView {
    String getImageView();
}
