package no.ntnu.henrbjoh.mappe3.patientRegister.factory.menu;

public interface Menu {
    String getMenu();
}
