package no.ntnu.henrbjoh.mappe3.patientRegister.factory.tableView;

public interface TableView {
    String getTableView();
}
