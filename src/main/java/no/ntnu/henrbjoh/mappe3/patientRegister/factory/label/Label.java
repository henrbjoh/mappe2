package no.ntnu.henrbjoh.mappe3.patientRegister.factory.label;

public interface Label {
    String getLabel();
}
