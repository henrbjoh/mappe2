package no.ntnu.henrbjoh.mappe3.patientRegister;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class takes care of all the reading/writing program-data to files
 */
public class Filehandler {
    private final File FILE;

    /**
     * Constructor, takes the filepath to where the patient-data will be stored on the computer
     * @param path path to data-file
     * @throws IOException if the program is unable to create the data-file
     */
    public Filehandler(String path) throws IOException {
        this.FILE = new File(path);

        if(!FILE.exists()) {
            boolean fileCreated = FILE.createNewFile();
            if(!fileCreated) {
                throw new IOException("Unable to create file " + FILE);
            }
        }
    }

    /**
     * Saves the patients in a list to the data-file
     * @param patientList the list of Patient-objects to be saved to file
     * @throws IOException if some of the objects are not correct
     */
    public void savePatientsToFile(Object[] patientList) throws IOException {
        saveToFile(patientList, this.FILE);
    }

    /**
     * Makes a new .csv-file with all the data in a given list of Patient-objects
     * @param patientList list of Patient-objects
     * @param file the file which the data will be stored to
     * @throws IOException if some of the objects are not correct
     */
    public void exportToFile(Object[] patientList, File file) throws IOException {
        saveToFile(patientList,file);
    }

    /**
     * Takes a list of Objects and splits it up to an Arraylist inside an Arraylist. Each index in Arraylist 1
     * contains an ArrayList of the data for each Patient
     * @param patientList Object-list with Patient-objects
     * @param file the fil which the patient-data will be stored to
     * @throws IllegalArgumentException if some of the objects are not correct
     * @throws IOException if the program is unable to write to the given file
     */
    public void saveToFile(Object[] patientList, File file) throws IllegalArgumentException, IOException {
        ArrayList<ArrayList<String>> fileData = new ArrayList<>();

        for(Object o : patientList) {
            if(o instanceof Patient) {
                Patient p = (Patient) o;
                String patientFirstName = p.getFirstName() + "";
                String patientLastName = p.getLastName() + "";
                String patientGeneralPractitioner = p.getGeneralPartitioner() + "";
                String patientSocialSecurityNumber = p.getSocialSecurityNumber();

                ArrayList<String> patientData = new ArrayList<>();
                patientData.add(0, patientFirstName);
                patientData.add(1, patientLastName);
                patientData.add(2, patientGeneralPractitioner);
                patientData.add(3, patientSocialSecurityNumber);

                fileData.add(patientData);
            } else {
                throw new IllegalArgumentException("Attempted to save object type " + o.getClass() + " as a Patient");
            }
        }
        this.saveRawFileDataToFile(fileData, file);
    }

    /**
     * Takes the list inside of a list from saveToFile() and writes it to the given file
     * @param fileData the Arraylist inside an Arraylist with patient-data
     * @param file the file which the data will be written to
     * @throws IOException if the program is unable to write to the given file
     */
    private void saveRawFileDataToFile(ArrayList<ArrayList<String>> fileData, File file) throws IOException {
        boolean dataEmpty = fileData.isEmpty();

        try(PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(file)))) {

            if (dataEmpty) {
                printWriter.write("");
                return;
            }

            for (ArrayList<String> lineData : fileData) {
                String line = "";

                for (String data : lineData) {
                    line += (data + ";");
                }

                printWriter.println(line);
            }
        }
    }

    /**
     * Returns a list of Patient-objects from the data-file to the program
     * @return Arraylist with Patient-objects
     * @throws IOException if the file with the data is not found
     */
    public ArrayList<Patient> getDataAsPatientsFromAppData() throws IOException {
        return getDataAsPatientObjects(this.FILE);
    }

    /**
     * Returns a list of Patient-objects from the import-file given as a parameter
     * @param file file imported trough the program with Patient-datas
     * @return Arraylist with Patient-objects
     * @throws IOException if the file with the data is not found
     */
    public ArrayList<Patient> getDataAsPatientsFromImportFile(File file) throws IOException {
        return getDataAsPatientObjects(file);
    }


    /**
     * Makes an Arraylist of Patient-objects from a given file. The method uses getRawFileDataFromFile() to
     * split the data in the file to strings and then creates Patient-objects which is added to a new list
     * @param file the file which the data is stored on
     * @return Arraylist with Patient-objects
     * @throws IOException if getRawFileDataFromFile() can not read data from the file
     */
    public ArrayList<Patient> getDataAsPatientObjects(File file) throws IOException {
        ArrayList<Patient> patients = new ArrayList<>();
        ArrayList<ArrayList<String>> fileData = this.getRawFileDataFromFile(file);

        for (ArrayList<String> patientData : fileData) {
            String firstName = patientData.get(0);
            String lastName = patientData.get(1);
            String generalPractitioner = patientData.get(2);
            String socialSecurityNumber = patientData.get(3);

            try {
                Long.parseLong(socialSecurityNumber);//Parses to check if social security number is only numbers, Will throw Exception if not

                Patient newPatient = new Patient(firstName, lastName, generalPractitioner, socialSecurityNumber);
                patients.add(newPatient);
            }catch (Exception ignored){
            }
        }
        return patients;
    }

    /**
     * Gives the program an ArrayList inside an Arraylist containing Patient-data as Strings from a given file
     * @param file file which the method will split up the data to later create Patient-objects
     * @return A list of lists with patient-data as Strings
     * @throws IOException if the file with the data is not found
     */
    protected ArrayList<ArrayList<String>> getRawFileDataFromFile(File file) throws IOException {
        ArrayList<ArrayList<String>> fileData = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))){
            String line;
            while((line = bufferedReader.readLine()) != null){
                ArrayList<String> values = new ArrayList<>(Arrays.asList(line.split(";")));
                fileData.add(values);
            }
        }
        return fileData;
    }

}
