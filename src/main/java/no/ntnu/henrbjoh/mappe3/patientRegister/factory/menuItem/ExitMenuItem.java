package no.ntnu.henrbjoh.mappe3.patientRegister.factory.menuItem;

public class ExitMenuItem implements MenuItem{
    @Override
    public String getMenuItem() {
        return "exit";
    }
}
