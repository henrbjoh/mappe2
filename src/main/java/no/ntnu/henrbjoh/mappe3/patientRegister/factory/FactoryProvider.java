package no.ntnu.henrbjoh.mappe3.patientRegister.factory;

import no.ntnu.henrbjoh.mappe3.patientRegister.factory.imageView.ImageViewFactory;
import no.ntnu.henrbjoh.mappe3.patientRegister.factory.label.LabelFactory;
import no.ntnu.henrbjoh.mappe3.patientRegister.factory.menu.MenuFactory;
import no.ntnu.henrbjoh.mappe3.patientRegister.factory.menuItem.MenuItemFactory;
import no.ntnu.henrbjoh.mappe3.patientRegister.factory.tableColumn.TableColumnFactory;
import no.ntnu.henrbjoh.mappe3.patientRegister.factory.tableView.TableViewFactory;

public class FactoryProvider {
    public static AbstractFactory<?> getFactory(String choice) {
        if ("ImageView".equalsIgnoreCase(choice)) {
            return new ImageViewFactory();
        } else if ("Label".equalsIgnoreCase(choice)) {
            return new LabelFactory();
        } else if ("Menu".equalsIgnoreCase(choice)) {
            return new MenuFactory();
        } else if ("MenuItem".equalsIgnoreCase(choice)) {
            return new MenuItemFactory();
        } else if ("TableColumn".equalsIgnoreCase(choice)) {
            return new TableColumnFactory();
        } else if ("TableView".equalsIgnoreCase(choice)) {
            return new TableViewFactory();
        }
        
        return null;
    }
}
