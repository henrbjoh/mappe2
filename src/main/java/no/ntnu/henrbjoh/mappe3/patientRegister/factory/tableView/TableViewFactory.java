package no.ntnu.henrbjoh.mappe3.patientRegister.factory.tableView;

import no.ntnu.henrbjoh.mappe3.patientRegister.factory.AbstractFactory;
import no.ntnu.henrbjoh.mappe3.patientRegister.factory.menu.EditMenu;
import no.ntnu.henrbjoh.mappe3.patientRegister.factory.menu.FileMenu;
import no.ntnu.henrbjoh.mappe3.patientRegister.factory.menu.HelpMenu;
import no.ntnu.henrbjoh.mappe3.patientRegister.factory.menu.Menu;

public class TableViewFactory implements AbstractFactory<TableView> {
    @Override
    public TableView create(String type) {
        if ("patientTableView".equalsIgnoreCase(type)) {
            return new PatientTableView();
        }
        return null;
    }
}
