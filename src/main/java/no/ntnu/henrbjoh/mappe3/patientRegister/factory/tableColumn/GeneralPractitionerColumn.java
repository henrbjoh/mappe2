package no.ntnu.henrbjoh.mappe3.patientRegister.factory.tableColumn;

public class GeneralPractitionerColumn implements TableColumn{
    @Override
    public String getTableColumn() {
        return "generalPractitionerColumn";
    }
}
