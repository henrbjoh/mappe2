package no.ntnu.henrbjoh.mappe3.patientRegister;

/**
 * Class to represent a single patient
 */
public class Patient {
    private final String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPartitioner;

    public Patient(String firstName, String lastName, String generalPartitioner, String socialSecurityNumber){
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.generalPartitioner = generalPartitioner;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGeneralPartitioner() {
        return generalPartitioner;
    }

    public void setGeneralPartitioner(String generalPartitioner) {
        this.generalPartitioner = generalPartitioner;
    }
}
