package no.ntnu.henrbjoh.mappe3.patientRegister;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

/**
 * This class takes care of all the actions and inputs in the GUI, It uses a .FXML-file to build the main window, and
 * makes the pop-up-windows in the representative methods.
 */
public class GUIController{
    private PatientManager patientManager;
    @FXML
    public TableView<Patient> patientTableView;
    public TableColumn<Patient, String> firstNameColumn;
    public TableColumn<Patient, String> lastNameColumn;
    public TableColumn<Patient, String> generalPractitionerColumn;
    public TableColumn<Patient, String> socialSecurityNumberColumn;
    public Label statusLabel;
    public ImageView addImageView;
    public ImageView editImageView;
    public ImageView removeImageView;
    public MenuItem addNewMenuItem;
    public MenuItem editNewMenuItem;
    public MenuItem removeNewMenuItem;


    /**
     * Constructor, makes a instance of the patientManager with path to where the data will be stored, Shows an errormessage if it fails
     */
    public GUIController(){
        try {
            patientManager = new PatientManager(System.getProperty("user.home")+"/.patientRegister.csv");
        } catch (IOException e) {
            e.printStackTrace();
            errorDialog("Unable to load the Patientregister");
        }
    }

    /**
     * Sets the different tableView-columns to their designated property.
     */
    @FXML
    public void initialize(){
        //sets the columns in the tableView
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPartitioner"));
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        //Fill tableView with the data on file when program starts
        fillTableView();
        //Makes the images to set on add, edit and delete
        Image addImage = new Image(Objects.requireNonNull(getClass().getResourceAsStream("images/add.png")));
        Image editImage = new Image(Objects.requireNonNull(getClass().getResourceAsStream("images/edit.png")));
        Image removeImage = new Image(Objects.requireNonNull(getClass().getResourceAsStream("images/remove.png")));
        //Sets the images and adds mouse-events to all of them
        addImageView.setImage(addImage);
        editImageView.setImage(editImage);
        removeImageView.setImage(removeImage);
        addImageView.setOnMouseClicked(e -> addNewPatient());
        editImageView.setOnMouseClicked(e -> editPatient());
        removeImageView.setOnMouseClicked(e -> removePatient());
        //Adds images to the menu items in the top menu
        ImageView addNewMenuItemImage = new ImageView(addImage);
        addNewMenuItemImage.setFitWidth(15);
        addNewMenuItemImage.setFitHeight(15);
        addNewMenuItem.setGraphic(addNewMenuItemImage);
        ImageView editNewMenuItemImage = new ImageView(editImage);
        editNewMenuItemImage.setFitWidth(15);
        editNewMenuItemImage.setFitHeight(15);
        editNewMenuItem.setGraphic(editNewMenuItemImage);
        ImageView removeNewMenuItemImage = new ImageView(removeImage);
        removeNewMenuItemImage.setFitWidth(15);
        removeNewMenuItemImage.setFitHeight(15);
        removeNewMenuItem.setGraphic(removeNewMenuItemImage);
    }

    /**
     * Makes an Observable list from the Arraylist from patientManager and fills the
     * tableview with the data.
     */
    @FXML
    public void fillTableView(){
        patientTableView.getItems().clear();
        ArrayList<Patient> patients = patientManager.getPatients();
        ObservableList<Patient> observableList = FXCollections.observableList(patients);
        patientTableView.setItems(observableList);
    }

    /**
     * pop-up dialog that shows about-info of the application
     */
    @FXML
    public void aboutDialog(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText("Patientregister");
        alert.setContentText("A brilliant application created by \n Henrik Johnsrud \n 26.04.2021");
        alert.showAndWait();
    }

    /**
     * Method takes the chosen patient in the tableview, if there are one.
     * And if the user confirms, deletes the selected patient
     */
    @FXML
    public void removePatient(){
        Patient selectedPatient = patientTableView.getSelectionModel().getSelectedItem();
        if (selectedPatient==null){
            errorDialog("You must select a patient to remove");
            return;
        }
        if (confirmDialog("Delete confirmation", "Are you sure you want to delete this patient?")) {
            if (patientManager.removePatient(selectedPatient.getSocialSecurityNumber())){
                setStatusLabel(selectedPatient.getFirstName() + " is removed from register");
            }
            else{
                setStatusLabel("Something went wrong, could not remove patient");
            }
        }
        savePatientsToFile();
        fillTableView();
    }

    /**
     * Asks for the users confirmation, then saves to file and closes the program
     */
    @FXML
    public void exitProgram(){
        if (confirmDialog("Exit program?", "Are you sure want to exit the program?")) {
            savePatientsToFile();
            System.exit(0);
        }
    }

    /**
     * Saves the patients to the program-data-file
     */
    private void savePatientsToFile(){
        try {
            patientManager.savePatientsToFile();
        }catch (IOException e){
            errorDialog("Could not save to file");
        }
    }

    /**
     * Error-dialog that is shown to the user if something goes wrong somewhere in the program
     * @param message the message to be shown to the user
     */
    public void errorDialog(String message){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);

        alert.setTitle("Error message");
        alert.setHeaderText("Error");
        alert.setContentText(message);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            alert.close();
        }
    }

    /**
     * Takes a header and a message and shows the user a confirm-dialog
     * @param header title of the dialog
     * @param message message in the dialog
     * @return boolean, true if user selects "OK" and false if not
     */
    public boolean confirmDialog(String header, String message){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText(header);
        alert.setContentText(message);

        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }

    /**
     * Methods to handle the import of a file. User is shown the fileChooser and picks the file he wants to import
     * if the import is successful, the user will get this in the statusbar, and the list will update
     * If not, he will get an error-message
     * The user will pnly be able to choose a .csv-file to import
     */
    @FXML
    public void importFile(){
        File file = showFileChooser("Import file");
        try {
            patientManager.importFile(file);
            fillTableView();
            setStatusLabel("Patients successfully added to the register");
        } catch (IOException e) {
            errorDialog("Could not import file, try again");
            setStatusLabel("Could not import file, try again");
        }
    }

    /**
     * Methods to handle the export of a file. User is shown the fileChooser and chooses where to be saved and what the name of the file will be.
     * if the export is successful, the user will get this in the statusbar
     * If not, he will get an error-message
     */
    @FXML
    public void exportFile(){
        File file = showFileChooser("Export file");
        try {
            patientManager.exportFile(file);
            setStatusLabel("Patient-list successfully exported to: "+file.getName());
        } catch (IOException e) {
            errorDialog("Could not export file, try again");
            setStatusLabel("Could not export file, try again");
        }
    }

    /**
     * Shows the FileChooser, and limits the types of files available to only .csv. If the user if exporting he will get a
     * SaveDialog and if he is going to import from file he will get a OpenDialog
     * @param title title of window, also used to choose what kind of fileChooser user get
     * @return The file that user chooses
     */
    @FXML
    public File showFileChooser(String title){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));//opens in user-folder for the first time

        Window stage = patientTableView.getScene().getWindow();
        fileChooser.setTitle("Export file");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("csv file","*.csv"));
        if (title.equals("Import file")) {
            return fileChooser.showOpenDialog(stage);
        }
        return fileChooser.showSaveDialog(stage);
    }

    /**
     * Method takes the chosen patient in the tableview, if there are one.
     * The user then does whatever changes he wants (social security number can not be changed) and then saved the changes done
     */
    @FXML
    public void editPatient(){
        Patient selectedPatient = patientTableView.getSelectionModel().getSelectedItem();
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Add new patient");

        if (selectedPatient==null){
            errorDialog("You must select a patient to edit");
            window.close();
            return;
        }

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10,10,10,10));
        grid.setVgap(8);
        grid.setHgap(10);

        Label firstNameLabel = new Label("Firstname:");
        GridPane.setConstraints(firstNameLabel,0,0);
        TextField firstNameInput = new TextField();
        firstNameInput.setPromptText("firstname..");
        firstNameInput.setText(selectedPatient.getFirstName());
        GridPane.setConstraints(firstNameInput,1,0);

        Label lastNameLabel = new Label("Last Name:");
        GridPane.setConstraints(lastNameLabel,0,1);
        TextField lastNameInput = new TextField();
        lastNameInput.setPromptText("lastname..");
        lastNameInput.setText(selectedPatient.getLastName());
        GridPane.setConstraints(lastNameInput,1,1);

        Label generalPractitionerLabel = new Label("General practitioner:");
        GridPane.setConstraints(generalPractitionerLabel,0,2);
        TextField generalPractitionerInput = new TextField();
        generalPractitionerInput.setPromptText("general practitioner..");
        generalPractitionerInput.setText(selectedPatient.getGeneralPartitioner());
        GridPane.setConstraints(generalPractitionerInput,1,2);

        Label SSNLabel = new Label("Social security number: ");
        GridPane.setConstraints(SSNLabel,0,3);
        Label SSN = new Label(""+selectedPatient.getSocialSecurityNumber());
        GridPane.setConstraints(SSN,1,3);

        Button addButton = new Button("Save changes");
        GridPane.setConstraints(addButton,0,4);
        addButton.setOnAction(e -> {
            if (firstNameInput.getText().equals("") || lastNameInput.getText().equals("") || generalPractitionerInput.getText().equals("")){
                errorDialog("You must fill in all the input-fields");
                return;
            }

            selectedPatient.setFirstName(firstNameInput.getText());
            selectedPatient.setLastName(lastNameInput.getText());
            selectedPatient.setGeneralPartitioner(generalPractitionerInput.getText());
            window.close();
            savePatientsToFile();
            fillTableView();
            setStatusLabel("Changes saved");
        });
        Button exitButton = new Button("Cancel");
        GridPane.setConstraints(exitButton,1,4);
        exitButton.setOnAction(e -> window.close());

        grid.getChildren().addAll(firstNameInput,firstNameLabel,lastNameInput,lastNameLabel,generalPractitionerInput,generalPractitionerLabel,SSN,SSNLabel,exitButton,addButton);
        Scene scene = new Scene(grid,300,200);
        window.setScene(scene);
        window.showAndWait();

    }

    /**
     * Shows the user a pop-up window with input-fields to type in info of the new patient
     * Will not save if any inputs are blank or if the social security number is typed in wrong
     */
    @FXML
    public void addNewPatient(){
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Add new patient");

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10,10,10,10));
        grid.setVgap(8);
        grid.setHgap(10);

        Label firstNameLabel = new Label("Firstname:");
        GridPane.setConstraints(firstNameLabel,0,0);
        TextField firstNameInput = new TextField();
        firstNameInput.setPromptText("firstname..");
        GridPane.setConstraints(firstNameInput,1,0);

        Label lastNameLabel = new Label("Last Name:");
        GridPane.setConstraints(lastNameLabel,0,1);
        TextField lastNameInput = new TextField();
        lastNameInput.setPromptText("lastname..");
        GridPane.setConstraints(lastNameInput,1,1);

        Label generalPractitionerLabel = new Label("General practitioner:");
        GridPane.setConstraints(generalPractitionerLabel,0,2);
        TextField generalPractitionerInput = new TextField();
        generalPractitionerInput.setPromptText("general practitioner..");
        GridPane.setConstraints(generalPractitionerInput,1,2);

        Label SSNLabel = new Label("Social security number: ");
        GridPane.setConstraints(SSNLabel,0,3);
        TextField SSNInput = new TextField();
        SSNInput.setPromptText("ddmmyyxxxxx");
        GridPane.setConstraints(SSNInput,1,3);

        Button addButton = new Button("Add patient");
        GridPane.setConstraints(addButton,0,4);
        addButton.setOnAction(e -> {
            int numberOfDigitsSSN = Math.toIntExact(SSNInput.getText().chars().mapToObj(i -> (char)i).count());
            try {
                Long.parseLong(SSNInput.getText());
            }catch (NumberFormatException n){
                errorDialog("The social security number must contain only numbers");
                return;
            }
            if (firstNameInput.getText().equals("") || lastNameInput.getText().equals("") || generalPractitionerInput.getText().equals("") || SSNInput.getText().equals("")){
                errorDialog("You must fill in all the input-fields");
                return;
            }
            else if (numberOfDigitsSSN!=11){
                errorDialog("The social security number must contain 11 digits");
                return;
            }
            else if (patientManager.getPatients().stream().anyMatch(p -> p.getSocialSecurityNumber().equals(SSNInput.getText()))){
                errorDialog("There are already a patient registered with the same social security number");
                return;
            }

            Patient newPatient =  new Patient(firstNameInput.getText(),lastNameInput.getText(),generalPractitionerInput.getText(),SSNInput.getText());
            if (patientManager.addNewPatient(newPatient)){
                setStatusLabel(firstNameInput.getText() + " is added to the patient-register");
            }
            else {
                setStatusLabel("Something failed, could not add patient");
            }

            window.close();
            savePatientsToFile();
            fillTableView();
        });
        Button exitButton = new Button("Cancel");
        GridPane.setConstraints(exitButton,1,4);
        exitButton.setOnAction(e -> window.close());

        grid.getChildren().addAll(firstNameInput,firstNameLabel,lastNameInput,lastNameLabel,generalPractitionerInput,generalPractitionerLabel,SSNInput,SSNLabel,exitButton,addButton);

        Scene scene = new Scene(grid,300,200);
        window.setScene(scene);
        window.showAndWait();
    }

    /**
     * Updates the status-bar at the bottom of the application
     * @param message the message to be shown after the "Status:"
     */
    private void setStatusLabel(String message){
        statusLabel.setText("Status: "+message);
    }
}
