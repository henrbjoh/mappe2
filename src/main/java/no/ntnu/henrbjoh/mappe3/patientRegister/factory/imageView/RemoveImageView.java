package no.ntnu.henrbjoh.mappe3.patientRegister.factory.imageView;

public class RemoveImageView implements ImageView{
    @Override
    public String getImageView(){
        return "removeImageView";
    }
}
