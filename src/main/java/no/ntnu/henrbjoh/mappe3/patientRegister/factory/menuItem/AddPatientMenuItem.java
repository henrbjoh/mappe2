package no.ntnu.henrbjoh.mappe3.patientRegister.factory.menuItem;

public class AddPatientMenuItem  implements MenuItem{
    @Override
    public String getMenuItem() {
        return "addPatient";
    }
}
