package no.ntnu.henrbjoh.mappe3.patientRegister.factory.tableColumn;

public interface TableColumn {
    String getTableColumn();
}
