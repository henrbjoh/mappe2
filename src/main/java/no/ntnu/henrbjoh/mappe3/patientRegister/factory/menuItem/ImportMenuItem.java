package no.ntnu.henrbjoh.mappe3.patientRegister.factory.menuItem;

public class ImportMenuItem implements MenuItem{
    @Override
    public String getMenuItem() {
        return "import";
    }
}
