package no.ntnu.henrbjoh.mappe3.patientRegister.factory.menuItem;

import no.ntnu.henrbjoh.mappe3.patientRegister.factory.AbstractFactory;

public class MenuItemFactory implements AbstractFactory<MenuItem> {
    @Override
    public MenuItem create(String type) {
        if ("about".equalsIgnoreCase(type)) {
            return new AboutMenuItem();
        } else if ("addPatient".equalsIgnoreCase(type)) {
            return new AddPatientMenuItem();
        } else if ("editPatient".equalsIgnoreCase(type)) {
            return new EditPatientMenuItem();
        } else if ("exit".equalsIgnoreCase(type)) {
            return new ExitMenuItem();
        } else if ("export".equalsIgnoreCase(type)) {
            return new ExportMenuItem();
        } else if ("import".equalsIgnoreCase(type)) {
            return new ImportMenuItem();
        } else if ("remove".equalsIgnoreCase(type)) {
            return new RemoveMenuItem();
        }
        return null;
    }
}
