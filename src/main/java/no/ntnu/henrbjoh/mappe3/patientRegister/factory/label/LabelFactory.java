package no.ntnu.henrbjoh.mappe3.patientRegister.factory.label;

import no.ntnu.henrbjoh.mappe3.patientRegister.factory.AbstractFactory;
import no.ntnu.henrbjoh.mappe3.patientRegister.factory.menu.EditMenu;
import no.ntnu.henrbjoh.mappe3.patientRegister.factory.menu.FileMenu;
import no.ntnu.henrbjoh.mappe3.patientRegister.factory.menu.HelpMenu;
import no.ntnu.henrbjoh.mappe3.patientRegister.factory.menu.Menu;

public class LabelFactory implements AbstractFactory<Label> {
    @Override
    public Label create(String type) {
        if ("statusLabel".equalsIgnoreCase(type)) {
            return new StatusLabel();
        }
        return null;
    }
}
